package com.cafebazaar.zonex.data.persistence

import android.content.Context
import androidx.room.Database
import androidx.room.Room.databaseBuilder
import androidx.room.RoomDatabase
import com.cafebazaar.zonex.domain.VenueEntity

/**
 * Created by Mina Alamolhoda ( minaalemalhoda@gmail.com ) on 2019-09-22.
 */
@Database(entities = [VenueEntity::class], version = 1)
abstract class AppDataBase : RoomDatabase() {
    abstract fun venueDao(): VenueDao

    companion object {

        @JvmStatic
        @Volatile
        private var INSTANCE: AppDataBase? = null

        @JvmStatic
        fun getInstance(context: Context): AppDataBase =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
            }

        private fun buildDatabase(context: Context) =
            databaseBuilder(context.applicationContext,
                AppDataBase::class.java, "zonex.db")
                .build()
    }
}
