package com.cafebazaar.zonex.data

import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Created by Mina Alamolhoda ( minaalemalhoda@gmail.com ) on 2019-09-19.
 */

interface VenueApi {

    @GET("venues/explore")
    suspend fun getVenues(
        @Query("offset") offset: Int,
        @Query("limit") limit: Int,
        @Query("ll") currentLocation: String
    ): ApiResponse<VenuesResponseDto>

    @GET("venues/{venueId}")
    suspend fun getVenueDetail(@Path("venueId") venueId: String) : ApiResponse<VenueDetailResponseDto>
}