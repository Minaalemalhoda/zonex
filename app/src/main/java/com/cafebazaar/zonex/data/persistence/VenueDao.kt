package com.cafebazaar.zonex.data.persistence

import androidx.lifecycle.LiveData
import androidx.room.*
import com.cafebazaar.zonex.domain.VenueEntity

/**
 * Created by Mina Alamolhoda ( minaalemalhoda@gmail.com ) on 2019-09-22.
 */
@Dao
interface VenueDao {

    @Query("DELETE FROM VenueEntity")
    suspend fun deleteAll()

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(venues: List<VenueEntity>)

    @Query("SELECT * FROM VenueEntity")
    fun getAllVenues(): LiveData<List<VenueEntity>>

    @Transaction
    suspend fun updateVenues(venues: List<VenueEntity>) {
        deleteAll()
        insert(venues)
    }
}