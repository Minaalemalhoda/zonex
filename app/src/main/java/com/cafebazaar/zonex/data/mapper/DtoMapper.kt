package com.cafebazaar.zonex.data.mapper

import com.cafebazaar.zonex.data.RecommendedObjectDto
import com.cafebazaar.zonex.data.VenueCategoryDto
import com.cafebazaar.zonex.data.VenueDetailDto
import com.cafebazaar.zonex.data.VenueLocationDto
import com.cafebazaar.zonex.domain.*

/**
 * Created by Mina Alamolhoda ( minaalemalhoda@gmail.com ) on 2019-09-22.
 */

fun List<RecommendedObjectDto>.toListOfVenue(): List<Venue> {
    return map { recommendedObjectDto ->
        recommendedObjectDto.venue.let { venue ->
            Venue(
                venue.id,
                venue.name,
                venue.categories.toVenueCategory(),
                venue.location.toVenueLocationInfo()
            )
        }
    }

}

private fun List<VenueCategoryDto>.toVenueCategory(): String {
    return first().type
}

private fun VenueLocationDto.toVenueLocationInfo(): VenueLocationInfo {
    return VenueLocationInfo(distance, address)
}

fun VenueDetailDto.toVenueDetail(): VenueDetail {
    return VenueDetail(
        id,
        name,
        location.toVenueLocationInfo(),
        canonicalUrl,
        Likes(likes.count, likes.summary),
        rating,
        Contact(contact?.phoneNumber, contact?.instagram)
    )
}