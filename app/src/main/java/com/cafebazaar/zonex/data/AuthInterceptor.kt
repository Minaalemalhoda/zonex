package com.cafebazaar.zonex.data

import okhttp3.Interceptor
import okhttp3.Response


/**
 * Created by Mina Alamolhoda ( minaalemalhoda@gmail.com ) on 2019-09-19.

 */
class AuthInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        var req = chain.request()
        val url =
            req.url().newBuilder()
                .addQueryParameter("client_id", CLIENT_ID)
                .addQueryParameter("client_secret", CLIENT_SECRET)
                .addQueryParameter("v", VERSION)
                .build()
        req = req.newBuilder().url(url).build()
        return chain.proceed(req)

    }

    companion object {
        const val CLIENT_ID = "QDFPBJAQLLDOYEIGKLU45UXJ1DG3XMW2MU23KSLEHD3JCSR5"
        const val CLIENT_SECRET = "ARW1YRMMIU4KSNGJT3HUPHJYBKP5MJ0MQS2WN3PY2IYGEL44"
        const val VERSION = "20180323"
    }
}