package com.cafebazaar.zonex.data.repository

import android.content.Context
import com.cafebazaar.zonex.domain.Location
import com.cafebazaar.zonex.domain.repository.LocationRepository
import com.cafebazaar.zonex.domain.toAndroidLocation
import com.cafebazaar.zonex.presentation.util.DEFAULT_FLOAT_VALUE
import com.cafebazaar.zonex.presentation.util.getFloat
import com.cafebazaar.zonex.presentation.util.saveFloat
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.channels.ReceiveChannel

/**
 * Created by Mina Alamolhoda ( minaalemalhoda@gmail.com ) on 2019-09-20.
 */
class LocationRepositoryImp(private val context: Context) : LocationRepository {

    /**
     * Note that the location is sent to channel if it meets the conditions defined
     * e.g. the distance between latest location and the new one is more than defined offset[LOCATION_UPDATE_OFFSET]
     * It's NOT always the latest user location, if you need the last known location @see [lastKnownLocation]
     */
    private val locationUpdates = ConflatedBroadcastChannel<Location>()

    private var lastKnownLocation: Location? = null

    override fun getLastCachedInfoLocation(): Location? {
        val latitude = context.getFloat(LATEST_LATITUDE_KEY)
        val longitude = context.getFloat(LATEST_LONGITUDE_KEY)
        if (latitude != null && latitude != DEFAULT_FLOAT_VALUE && longitude != null && longitude != DEFAULT_FLOAT_VALUE)
            return Location(latitude, longitude)
        return null
    }


    override fun submitLocation(newLocation: Location) {
        lastKnownLocation = newLocation
        val cachedLocation = getLastCachedInfoLocation()
        if (cachedLocation == null) {
            cacheLocation(newLocation)
            locationUpdates.offer(newLocation)
        } else {
            if (newLocation.getDistance(cachedLocation) > LOCATION_UPDATE_OFFSET)
                locationUpdates.offer(newLocation)
        }


    }

    private fun Location.getDistance(location: Location): Float {
        return this.toAndroidLocation().distanceTo(location.toAndroidLocation())
    }

    override fun getLocationUpdates(): ReceiveChannel<Location> {
        return locationUpdates.openSubscription()
    }

    override fun getLastKnownLocation() = lastKnownLocation


    companion object {
        const val LATEST_LATITUDE_KEY = "LATEST_LATITUDE_KEY"
        const val LATEST_LONGITUDE_KEY = "LATEST_LONGITUDE_KEY"
        const val LOCATION_UPDATE_OFFSET = 100
    }

    override fun cacheLocation(location: Location) {
        context.saveFloat(location.latitude,
            LATEST_LATITUDE_KEY
        )
        context.saveFloat(location.longitude,
            LATEST_LONGITUDE_KEY
        )
    }


}