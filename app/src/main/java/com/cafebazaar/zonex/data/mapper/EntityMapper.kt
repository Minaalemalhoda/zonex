package com.cafebazaar.zonex.data.mapper

import com.cafebazaar.zonex.domain.Venue
import com.cafebazaar.zonex.domain.VenueEntity
import com.cafebazaar.zonex.domain.VenueLocationInfo

/**
 * Created by Mina Alamolhoda ( minaalemalhoda@gmail.com ) on 2019-09-22.
 */

fun VenueEntity.toVenue() = Venue(id, name, category, VenueLocationInfo(distance, address))

fun Venue.toVenueEntity() =
    VenueEntity(id, name, locationInfo.distance, locationInfo.address, category)