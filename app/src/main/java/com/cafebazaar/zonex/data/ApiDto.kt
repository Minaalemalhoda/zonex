package com.cafebazaar.zonex.data

import com.google.gson.annotations.SerializedName

/**
 * Created by Mina Alamolhoda ( minaalemalhoda@gmail.com ) on 2019-09-21.
 */

data class ApiResponse<T>(@SerializedName("meta") val meta: Meta, @SerializedName("response") val data: T)

data class Meta(@SerializedName("code") val code: Int, @SerializedName("errorType") val errorType: String?)

data class VenuesResponseDto(@SerializedName("groups") val recommendations: List<RecommendedGroupDto>)

data class RecommendedGroupDto(
    @SerializedName("type") val type: String, @SerializedName("name") val name: String,
    @SerializedName("items") val items: List<RecommendedObjectDto>
)

data class RecommendedObjectDto(@SerializedName("venue") val venue: VenueDto)

data class VenueDto(
    @SerializedName("id") val id: String,
    @SerializedName("name") val name: String,
    @SerializedName("location") val location: VenueLocationDto,
    @SerializedName("categories") val categories: List<VenueCategoryDto>
)

data class VenueLocationDto(@SerializedName("distance") val distance: Int, @SerializedName("address") val address: String?)

data class VenueCategoryDto(@SerializedName("name") val type: String)

data class VenueDetailDto(
    @SerializedName("id") val id: String,
    @SerializedName("name") val name: String,
    @SerializedName("canonicalUrl") val canonicalUrl: String,
    @SerializedName("likes") val likes: LikesDto,
    @SerializedName("rating") val rating: Float,
    @SerializedName("contact") val contact: ContactDto?,
    @SerializedName("location") val location: VenueLocationDto
)

data class LikesDto(@SerializedName("count") val count: Int, @SerializedName("summary") val summary: String)

data class ContactDto(@SerializedName("phone") val phoneNumber: String?, @SerializedName("instagram") val instagram: String?)

data class VenueDetailResponseDto(@SerializedName("venue") val venueDetail: VenueDetailDto)