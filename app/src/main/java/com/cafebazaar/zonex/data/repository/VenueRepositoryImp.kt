package com.cafebazaar.zonex.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.cafebazaar.zonex.data.VenueApi
import com.cafebazaar.zonex.data.mapper.toListOfVenue
import com.cafebazaar.zonex.data.mapper.toVenue
import com.cafebazaar.zonex.data.mapper.toVenueDetail
import com.cafebazaar.zonex.data.mapper.toVenueEntity
import com.cafebazaar.zonex.data.persistence.VenueDao
import com.cafebazaar.zonex.domain.Location
import com.cafebazaar.zonex.domain.Venue
import com.cafebazaar.zonex.domain.VenueDetail
import com.cafebazaar.zonex.domain.repository.VenueRepository

/**
 * Created by Mina Alamolhoda ( minaalemalhoda@gmail.com ) on 2019-09-22.
 */

class VenueRepositoryImp(
    private val api: VenueApi,
    private val venueDao: VenueDao

) : VenueRepository {
    /**
     * Cached venues from db
     */
    override val venues: LiveData<List<Venue>> =
        Transformations.map(venueDao.getAllVenues()) { entities ->
            entities.map {
                it.toVenue()
            }
        }


    override suspend fun getVenues(
        currentLocation: Location,
        offset: Int,
        limit: Int
    ): List<Venue> {
        return api.getVenues(offset, limit, currentLocation.toString()).data.recommendations.first()
            .items.toListOfVenue()
    }

    override suspend fun insertCachedVenues(venues: List<Venue>) {
        venueDao.insert(venues = venues.map { it.toVenueEntity() })
    }

    override suspend fun updateCachedVenue(venues: List<Venue>) {
        venueDao.updateVenues(venues = venues.map { it.toVenueEntity() })
    }

    override suspend fun getVenueDetail(venueId: String): VenueDetail {
        return api.getVenueDetail(venueId).data.venueDetail.toVenueDetail()
    }


}