package com.cafebazaar.zonex.presentation

import android.app.Application
import com.cafebazaar.zonex.presentation.di.module.getZonexModules
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

/**
 * Created by Mina Alamolhoda ( minaalemalhoda@gmail.com ) on 2019-09-18.
 */

class ZonexApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        initializeKoin()
    }

    private fun initializeKoin() {
        startKoin {
            androidLogger()
            androidContext(this@ZonexApplication)
            modules(getZonexModules())
        }
    }


}