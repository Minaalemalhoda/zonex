package com.cafebazaar.zonex.presentation.service.microservice

import android.os.Handler
import kotlinx.coroutines.flow.Flow

/**
 * Created by Mina Alamolhoda ( minaalemalhoda@gmail.com ) on 2019-09-20.
 */
/**
 * Base class for micro services that provide essential statuses of device as a stream such as [GPS, CONNECTIVITY,...]
 * @sample [GpsMicroService] [ConnectivityStatusMicroService]
 */

interface UserStatusMicroService<STATUS> {

    val handler: Handler
    fun statuses(): Flow<STATUS>
    fun getCurrentStatus(): STATUS
}