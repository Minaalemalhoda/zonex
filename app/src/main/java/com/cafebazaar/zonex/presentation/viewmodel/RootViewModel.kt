package com.cafebazaar.zonex.presentation.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.cafebazaar.zonex.domain.ConnectivityStatus
import com.cafebazaar.zonex.domain.GpsStatus
import com.cafebazaar.zonex.presentation.service.microservice.ConnectivityStatusMicroService
import com.cafebazaar.zonex.presentation.service.microservice.GpsMicroService
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

/**
 * Created by Mina Alamolhoda ( minaalemalhoda@gmail.com ) on 2019-09-21.
 */
class RootViewModel(
    private val connectivityStatusMicroService: ConnectivityStatusMicroService,
    private val gpsMicroService: GpsMicroService
) : BaseViewModel() {


    val gpsStatus: MutableLiveData<GpsStatus> by lazy {
        listenToGpsStatus()
        MutableLiveData(gpsMicroService.getCurrentStatus())
    }

    val connectivityStatus: MutableLiveData<ConnectivityStatus> by lazy {
        listenToConnectivityStatus()
        MutableLiveData(connectivityStatusMicroService.getCurrentStatus())
    }


    private fun listenToConnectivityStatus() {
        viewModelScope.launch {
            onBg {
                connectivityStatusMicroService.statuses().collect { status ->
                    when (status) {
                        ConnectivityStatus.CONNECT -> {
                            connectivityStatus.postValue(ConnectivityStatus.CONNECT)
                        }
                        else -> connectivityStatus.postValue(ConnectivityStatus.DISCONNECT)
                    }
                }
            }
        }
    }

    private fun listenToGpsStatus() {
        viewModelScope.launch {
            onBg {
                gpsMicroService.statuses().collect { status ->
                    when (status) {
                        GpsStatus.ON -> {
                            gpsStatus.postValue(GpsStatus.ON)
                        }
                        else -> gpsStatus.postValue(GpsStatus.OFF)
                    }
                }
            }
        }
    }


}