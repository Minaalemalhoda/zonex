package com.cafebazaar.zonex.presentation.ui.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.cafebazaar.zonex.R
import com.cafebazaar.zonex.domain.LoadableData.*
import com.cafebazaar.zonex.presentation.ui.adapter.VenueListAdapter
import com.cafebazaar.zonex.presentation.util.LoadMoreScrollListener
import com.cafebazaar.zonex.presentation.util.addLoadMoreListener
import com.cafebazaar.zonex.presentation.viewmodel.VenuesViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_venues_list.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel


class VenuesListFragment : Fragment() {

    private val viewModel: VenuesViewModel by sharedViewModel()

    lateinit var venuesAdapter: VenueListAdapter

    lateinit var loadMoreScrollListener: LoadMoreScrollListener

    private var errorSnackBar: Snackbar? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_venues_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpVenuesRecyclerView()
        enableSwipeToRefresh()
        listenToVenuesUpdate()
        listenToVenuesState()
        listenToVenueDetailState()
    }

    private fun listenToVenueDetailState() {
        viewModel.venueDetail.observe(this, Observer { state ->
            when (state.state) {
                LOADING -> {
                    hideError()
                    showLoading()
                }
                LOADED -> {
                    hideError()
                    hideLoading()
                    navigateToVenueDetailPage()
                }
                FAILED -> {
                    hideLoading()
                    showError(getString(R.string.venuedetail_error))
                }
                else -> {

                }
            }
        })
    }

    private fun navigateToVenueDetailPage() {
        view?.findNavController()?.navigate(R.id.venueDetailFragment)
    }

    private fun enableSwipeToRefresh() {
        swiperefreshlayout_venueslist.setOnRefreshListener {
            viewModel.forceUpdateVenues()
        }
    }

    private fun listenToVenuesState() {
        viewModel.state.observe(this, Observer {
            Log.d(this@VenuesListFragment.javaClass.name, "The new state is ${it.state}")
            when (it.state) {
                LOADING -> {
                    if (!swiperefreshlayout_venueslist.isRefreshing)
                        venuesAdapter.showLoading()
                }
                FAILED -> {
                    swiperefreshlayout_venueslist.isRefreshing = false
                    loadMoreScrollListener.canLoadMore = true
                    venuesAdapter.hideLoading()
                    venuesAdapter.showRetry()
                }
                LOADED -> {
                    swiperefreshlayout_venueslist.isRefreshing = false
                    loadMoreScrollListener.canLoadMore = true
                    venuesAdapter.hideLoading()
                    venuesAdapter.hideRetry()
                }
                NOT_INITIALIZED -> {
                    swiperefreshlayout_venueslist.isRefreshing = false
                    venuesAdapter.hideLoading()
                    venuesAdapter.showRetry()
                }
            }
        })

    }

    private fun setUpVenuesRecyclerView() {
        venuesAdapter = VenueListAdapter({
            viewModel.getVenueDetail(id)
        }, {
            viewModel.updateVenues()
        })
        recyclerview_venueslist_venues.adapter = venuesAdapter
        recyclerview_venueslist_venues.layoutManager = LinearLayoutManager(context)
        loadMoreScrollListener = recyclerview_venueslist_venues.addLoadMoreListener {
            loadMoreScrollListener.canLoadMore = false
            loadMoreVenues()
        }

    }

    private fun loadMoreVenues() {
        viewModel.updateVenues()
    }

    private fun listenToVenuesUpdate() {
        viewModel.venues.observe(this, Observer { newData ->
            Log.d(this.javaClass.name, "New venues are added to list....${newData.size}")
            venuesAdapter.submitList(newData.toMutableList())
        })
    }

    private fun showLoading() {
        progressbar_venuesList.visibility = View.VISIBLE
    }

    private fun hideLoading() {
        progressbar_venuesList.visibility = View.GONE
    }

    private fun showError(string: String) {
        errorSnackBar = Snackbar.make(container, string, Snackbar.LENGTH_LONG).apply {
            show()
        }
    }

    private fun hideError() {
        errorSnackBar?.dismiss()
    }

    override fun onDestroy() {
        hideError()
        super.onDestroy()
    }

}
