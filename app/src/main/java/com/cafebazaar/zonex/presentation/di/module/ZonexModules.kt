package com.cafebazaar.zonex.presentation.di.module

/**
 * Created by Mina Alamolhoda ( minaalemalhoda@gmail.com ) on 2019-09-18.
 */

fun getZonexModules() = listOf(networkModule, deviceInfoModule, applicationModule, venuesModule)