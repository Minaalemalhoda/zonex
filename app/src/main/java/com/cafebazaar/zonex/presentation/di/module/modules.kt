package com.cafebazaar.zonex.presentation.di.module

import com.cafebazaar.zonex.data.VenueApi
import com.cafebazaar.zonex.data.persistence.AppDataBase
import com.cafebazaar.zonex.domain.interactor.StartLocationTrackerService
import com.cafebazaar.zonex.domain.repository.LocationRepository
import com.cafebazaar.zonex.domain.repository.VenueRepository
import com.cafebazaar.zonex.data.repository.LocationRepositoryImp
import com.cafebazaar.zonex.data.repository.VenueRepositoryImp
import com.cafebazaar.zonex.domain.interactor.UpdateVenues
import com.cafebazaar.zonex.presentation.service.microservice.ConnectivityStatusMicroService
import com.cafebazaar.zonex.presentation.service.microservice.GpsMicroService
import com.cafebazaar.zonex.presentation.viewmodel.RootViewModel
import com.cafebazaar.zonex.presentation.viewmodel.VenuesViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit

/**
 * Created by Mina Alamolhoda ( minaalemalhoda@gmail.com ) on 2019-09-20.
 */

val deviceInfoModule = module {
    single {
        ConnectivityStatusMicroService(
            get()
        )
    }
    single { GpsMicroService(get()) }
}

val applicationModule = module {
    viewModel {
        RootViewModel(get(), get())
    }
    single<LocationRepository> { LocationRepositoryImp(get()) }
    factory { StartLocationTrackerService(get(), get(), get()) }
}

val venuesModule = module {
    single {
        get<Retrofit>().create(VenueApi::class.java)
    }
    single { AppDataBase.getInstance(get()).venueDao() }
    single<VenueRepository> { VenueRepositoryImp(get(), get()) }
    factory { UpdateVenues(get()) }
    viewModel {
        VenuesViewModel(get(),get(),get())
    }
}
