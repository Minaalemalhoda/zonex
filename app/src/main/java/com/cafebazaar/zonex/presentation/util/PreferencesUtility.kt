package com.cafebazaar.zonex.presentation.util

import android.content.Context
import android.content.SharedPreferences
import com.cafebazaar.zonex.BuildConfig

/**
 * Created by Mina Alamolhoda ( minaalemalhoda@gmail.com ) on 2019-09-20.
 */

fun Context.getPref(prefName: String? = null) = if(prefName == null) getDefaultPref() else getSharedPreferences(prefName, Context.MODE_PRIVATE)

const val DEFAULT_PREF_NAME = BuildConfig.APPLICATION_ID

const val DEFAULT_FLOAT_VALUE = -1f

const val DEFAULT_INT_VALUE = -1

fun Context.getDefaultPref(): SharedPreferences? = getSharedPreferences(DEFAULT_PREF_NAME, Context.MODE_PRIVATE)

fun Context.saveFloat(value: Float, prefKey: String, prefName: String? = null) {
    getPref(prefName)?.let {
        with(it.edit()) {
            putFloat(prefKey, value)
            apply()
        }
    }
}

fun Context.getFloat(prefKey: String, prefName: String? = null): Float? {
    return getPref(prefName)?.let {
        it.getFloat(prefKey, DEFAULT_FLOAT_VALUE)
    }
}

fun Context.saveInt(value: Int, prefKey: String, prefName: String? = null) {
    getPref(prefName)?.let {
        with(it.edit()) {
            putInt(prefKey, value)
            apply()
        }
    }
}

fun Context.getInt(prefKey: String, prefName: String? = null): Int? {
    return getPref(prefName)?.let {
        it.getInt(prefKey, DEFAULT_INT_VALUE)
    }
}