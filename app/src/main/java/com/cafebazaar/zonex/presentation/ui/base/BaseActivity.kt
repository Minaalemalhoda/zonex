package com.cafebazaar.zonex.presentation.ui.base

import androidx.appcompat.app.AppCompatActivity
import com.cafebazaar.zonex.presentation.util.CoroutineDispatcherProvider
import com.cafebazaar.zonex.presentation.util.CoroutineUtility
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.cancelChildren
import kotlinx.coroutines.withContext
import org.koin.android.ext.android.inject

/**
 * Created by Mina Alamolhoda ( minaalemalhoda@gmail.com ) on 2019-09-20.
 */

open class BaseActivity : AppCompatActivity() {

    val coroutineContexts = CoroutineUtility.coroutineDispatcherProvider()

    suspend inline fun <T> onUI(crossinline coroutine: suspend () -> T): T {
        return withContext(coroutineContexts.uiDispatcher()) {
            coroutine()
        }
    }

    suspend inline fun <T> onBg(crossinline coroutine: suspend () -> T): T {
        return withContext(coroutineContexts.bgDispatcher()) {
            coroutine()
        }
    }

}