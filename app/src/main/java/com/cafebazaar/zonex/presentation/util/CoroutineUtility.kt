package com.cafebazaar.zonex.presentation.util

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

/**
 * Created by Mina Alamolhoda ( minaalemalhoda@gmail.com ) on 2019-09-20.
 */
object CoroutineUtility {

    @JvmStatic
    fun coroutineDispatcherProvider() = object : CoroutineDispatcherProvider {
        override fun bgDispatcher(): CoroutineDispatcher {
            return Dispatchers.Default
        }

        override fun ioDispatcher(): CoroutineDispatcher {
            return Dispatchers.IO
        }

        override fun uiDispatcher(): CoroutineDispatcher {
            return Dispatchers.Main
        }

    }

}

interface CoroutineDispatcherProvider {
    fun bgDispatcher(): CoroutineDispatcher
    fun uiDispatcher(): CoroutineDispatcher
    fun ioDispatcher(): CoroutineDispatcher
}
