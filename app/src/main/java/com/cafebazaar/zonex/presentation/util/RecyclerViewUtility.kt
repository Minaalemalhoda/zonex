package com.cafebazaar.zonex.presentation.util

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

/**
 * Created by Mina Alamolhoda ( minaalemalhoda@gmail.com ) on 2019-09-23.
 */

/**
 * Adds pagination-like load-more capabilities to a RecyclerView.
 * @param loadMoreFunction: the function that is called when recyclerView's bottom is reached.
 * @param visibleItemsThreshold: number of items that will trigger the loadMoreFunction before reaching the bottom
 *
 */
typealias OnLoadMoreListener = (LoadMoreScrollListener) -> Unit

class LoadMoreScrollListener(
    var visibleItemsThreshold: Int = 2,
    private val loadMoreFunction: OnLoadMoreListener
) :
    RecyclerView.OnScrollListener() {
    var canLoadMore: Boolean = true
    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        if (!canLoadMore)
            return
        val shouldCallLoadMore: Boolean = when (val layoutManager = recyclerView.layoutManager) {
            is LinearLayoutManager -> {
                val lastVisibleItem = layoutManager.findLastVisibleItemPosition()
                val totalItems = layoutManager.itemCount
                totalItems <= lastVisibleItem + visibleItemsThreshold
            }
            else -> false

        }
        if (shouldCallLoadMore)
            loadMoreFunction(this)
    }
}

fun RecyclerView.addLoadMoreListener(
    visibleItemsThreshold: Int = 2,
    loadMoreFunction: OnLoadMoreListener
): LoadMoreScrollListener {
    val loadMoreScrollListener = LoadMoreScrollListener(
        visibleItemsThreshold,
        loadMoreFunction
    )
    addOnScrollListener(loadMoreScrollListener)
    return loadMoreScrollListener
}