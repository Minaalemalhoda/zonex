package com.cafebazaar.zonex.presentation.service.microservice

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Handler
import android.os.HandlerThread
import android.util.Log
import com.cafebazaar.zonex.domain.ConnectivityStatus
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.channels.sendBlocking
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow

/**
 * Created by Mina Alamolhoda ( minaalemalhoda@gmail.com ) on 2019-09-20.

 */
/**
 * As [getNetworkInfo] was deprecated in API level 23,
 * And also alternative method [getType], and types themselves, are now deprecated in API Level 28.
 * This class pick out the right solution based upon device OS API level.
 */
class ConnectivityStatusMicroService constructor(private val context: Context) :
    UserStatusMicroService<ConnectivityStatus> {

    override val handler = Handler(HandlerThread("Network Handler Thread").apply {
        start()
    }.looper)

    private val connectivityManager =
        (context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager)

    @Suppress("DEPRECATION")
    private val intentFilter = IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)

    /**
     * The main method that represents incoming connectivity status changes
     */
    override fun statuses(): Flow<ConnectivityStatus> {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) getNetworkCallbackResponseFlow() else getConnectivityReceiverResponseFlow()
    }

    /**
     * returns current connectivity status
     */
    override fun getCurrentStatus() =
        if (getConnectivityStatus()) ConnectivityStatus.CONNECT else ConnectivityStatus.DISCONNECT

    private fun getConnectivityStatus(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            connectivityManager.activeNetwork?.let { network ->
                connectivityManager.getNetworkCapabilities(network)?.apply {
                    return hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) || hasTransport(
                        NetworkCapabilities.TRANSPORT_WIFI
                    )
                }
            }
        } else {
            @Suppress("DEPRECATION")
            connectivityManager.activeNetworkInfo?.let {
                return it.isConnected
            }
        }

        return false
    }

    @SuppressLint("NewApi")
    private fun getNetworkCallbackResponseFlow(): Flow<ConnectivityStatus> = callbackFlow {

        val networkCallback = object : ConnectivityManager.NetworkCallback() {

            override fun onAvailable(network: Network) {
                super.onAvailable(network)
                Log.d(
                    this@ConnectivityStatusMicroService.javaClass.name,
                    "The connectivity status of device changed to connect from network callback."
                )
                sendBlocking(ConnectivityStatus.CONNECT)
            }

            override fun onLost(network: Network) {
                super.onLost(network)
                Log.d(
                    this@ConnectivityStatusMicroService.javaClass.name,
                    "The connectivity status of device changed to disconnect from network callback."
                )
                sendBlocking(ConnectivityStatus.DISCONNECT)
            }
        }

        connectivityManager.registerDefaultNetworkCallback(
            networkCallback
        )

        awaitClose {
            Log.d(this@ConnectivityStatusMicroService.javaClass.name, "The network callback has been unregistered.")
            connectivityManager.unregisterNetworkCallback(
                networkCallback
            )
        }
    }

    @Suppress("DEPRECATION")
    private fun getConnectivityReceiverResponseFlow(): Flow<ConnectivityStatus> = callbackFlow {
        val networkReceiver = object : BroadcastReceiver() {
            override fun onReceive(p0: Context, intent: Intent) {
                intent.action.takeIf { it == ConnectivityManager.CONNECTIVITY_ACTION }.let {
                    val isConnected = connectivityManager.activeNetworkInfo != null
                    if (isConnected) {
                        Log.d(this@ConnectivityStatusMicroService.javaClass.name, "The connectivity status of device changed to connect from network receiver.")
                        sendBlocking(ConnectivityStatus.CONNECT)
                    } else {
                        Log.d(this@ConnectivityStatusMicroService.javaClass.name, "The connectivity status of device changed to disconnect from network receiver.")
                        sendBlocking(ConnectivityStatus.DISCONNECT)
                    }

                }
            }
        }

        context.applicationContext.registerReceiver(
            networkReceiver,
            intentFilter,
            null,
            handler
        )

        awaitClose {
            Log.d(this@ConnectivityStatusMicroService.javaClass.name, "The network receiver has been unregistered.")
            context.applicationContext.unregisterReceiver(networkReceiver) }
    }

}