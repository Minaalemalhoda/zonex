package com.cafebazaar.zonex.presentation.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.cafebazaar.zonex.R
import com.cafebazaar.zonex.domain.Venue
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_venue.*

/**
 * Created by Mina Alamolhoda ( minaalemalhoda@gmail.com ) on 2019-09-23.
 */

class VenueListAdapter constructor(
    private val onVenueClicked: Venue.() -> Unit,
    private val onRetryButtonClicked: () -> Unit
) :
    ListAdapter<Venue, ViewHolder>(VenueDiffCalback) {

    private var isShowingLoading = false
    private var isShowingRetry = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return when (viewType) {
            VenueListAdapter.VenueItemType.LOADING.ordinal -> ViewHolder.LoadMoreViewHolder(
                getView(
                    R.layout.item_loading,
                    parent
                )
            )
            VenueListAdapter.VenueItemType.RETRY.ordinal -> ViewHolder.RetryViewHolder(
                getView(
                    R.layout.item_retry,
                    parent
                )
                , onRetryButtonClicked
            )
            else -> ViewHolder.VenueViewHolder(getView(R.layout.item_venue, parent), onVenueClicked)
        }
    }

    private fun getView(@LayoutRes layout: Int, parent: ViewGroup) =
        LayoutInflater.from(parent.context).inflate(
            layout,
            parent,
            false
        )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (holder is ViewHolder.VenueViewHolder) {
            holder.bindView(getItem(position))
        } else if (holder is ViewHolder.RetryViewHolder) {
            holder.bind()
        }
    }

    override fun getItemViewType(position: Int): Int {
        if (position == itemCount - 1)
            if (isShowingLoading)
                return VenueListAdapter.VenueItemType.LOADING.ordinal
            else if (isShowingRetry)
                return VenueListAdapter.VenueItemType.RETRY.ordinal

        return VenueListAdapter.VenueItemType.DEFAULT.ordinal
    }

    override fun submitList(list: MutableList<Venue>?) {
        isShowingLoading = false
        isShowingRetry = false
        super.submitList(list)
    }


    override fun getItemCount() =
        (if (isShowingLoading || isShowingRetry) 1 else 0) + super.getItemCount()

    fun showLoading() {
        if (isShowingLoading)
            return
        hideRetry()
        isShowingLoading = true
        notifyItemInserted(itemCount - 1)
    }

    fun hideLoading() {
        if (isShowingLoading) {
            isShowingLoading = false
            notifyItemRemoved(itemCount)
        }
    }

    fun showRetry() {
        if (isShowingRetry)
            return
        hideLoading()
        isShowingRetry = true
        notifyItemInserted(itemCount - 1)
    }

    fun hideRetry() {
        if (isShowingRetry) {
            isShowingRetry = false
            notifyItemRemoved(itemCount)
        }
    }

    enum class VenueItemType {
        RETRY,
        LOADING,
        DEFAULT
    }
}


sealed class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), LayoutContainer {


    class VenueViewHolder(itemView: View, private val onVenueClicked: Venue.() -> Unit) :
        ViewHolder(itemView) {
        override val containerView: View?
            get() = itemView

        fun bindView(venue: Venue) {
            containerView?.setOnClickListener {
                onVenueClicked.invoke(venue)
            }
            textview_venue_title.text = venue.name
            textview_venue_title.text =
                itemView.context.getString(R.string.venue_name, adapterPosition + 1, venue.name)
            textView_venue_category.text = venue.category
            textView_venue_distance.text = itemView.context.getString(
                R.string.venue_distance, venue.locationInfo.distance.toString()
            )
            imageview_venue_category.setImageResource(getCategoryDrawable(venue.category))
            textview_venue_addresss.text = venue.locationInfo.address

        }

        private fun getCategoryDrawable(category: String): Int = when (category) {
            getString(R.string.venue_category_bakery) -> R.drawable.ic_croissant
            getString(R.string.venue_category_cafe) -> R.drawable.ic_coffee
            getString(R.string.venue_category_restaurant) -> R.drawable.ic_breakfast
            getString(R.string.venue_category_burger) -> R.drawable.ic_burger
            else -> R.drawable.ic_star
        }

        private fun getString(id: Int) = itemView.context.getString(id)

    }

    class LoadMoreViewHolder(itemView: View) : ViewHolder(itemView) {
        override val containerView: View?
            get() = itemView
    }


    class RetryViewHolder(itemView: View, private val onRetryButtonClicked: () -> Unit) :
        ViewHolder(itemView) {
        override val containerView: View?
            get() = itemView

        fun bind() {
            containerView?.setOnClickListener {
                onRetryButtonClicked.invoke()
            }
        }

    }


}


object VenueDiffCalback : DiffUtil.ItemCallback<Venue>() {
    override fun areItemsTheSame(oldItem: Venue, newItem: Venue): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Venue, newItem: Venue): Boolean {
        return oldItem == newItem
    }
}
