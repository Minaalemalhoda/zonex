package com.cafebazaar.zonex.presentation.service

import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.HandlerThread
import android.os.IBinder
import android.util.Log
import com.cafebazaar.zonex.domain.mapToZonexLocation
import com.cafebazaar.zonex.domain.repository.LocationRepository
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import org.koin.android.ext.android.inject

/**
 * Created by Mina Alamolhoda ( minaalemalhoda@gmail.com ) on 2019-09-19.
 */
/**
 * Background service using FusedLocation API, get current location through specific interval.
 * Start based on some circumstances: GPS gets enabled, Connectivity gets enabled,
 * And also the required permission[ACCESS_FINE_LOCATION] is granted.
 * @see [StartLocationTrackerService] interactor
 *
 */
class FusedLocationTrackerService : Service() {

    private lateinit var locationThread: HandlerThread
    private lateinit var locationCallback: LocationCallback
    private lateinit var locationRequest: LocationRequest

    private val locationRepository: LocationRepository by inject()


    override fun onBind(p0: Intent?): IBinder? = null


    override fun onCreate() {
        super.onCreate()
        locationThread = HandlerThread("Location Handler Thread").apply {
            start()
        }
        locationRequest = LocationRequest().let {
            it.interval = LOCATION_TRACKER_INTERVAL
            it.smallestDisplacement = 0f
            it.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            it.setFastestInterval(LOCATION_TRACKER_INTERVAL)
        }

        locationCallback = object : LocationCallback() {
            override fun onLocationResult(result: LocationResult?) {
                super.onLocationResult(result)
                result?.locations?.forEach { location ->
                    Log.d(
                        this@FusedLocationTrackerService.javaClass.name,
                        "The new location is: ${location.latitude}, ${location.longitude}"
                    )

                    locationRepository.submitLocation(location.mapToZonexLocation())
                }
            }
        }

    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        /**
         * The message queue of locationThread will be used to implement the callback mechanism.
         */
        Log.d(this@FusedLocationTrackerService.javaClass.name, "The location tracker service has been started successfully.")
        LocationServices.getFusedLocationProviderClient(this)
            .requestLocationUpdates(locationRequest, locationCallback, locationThread.looper)

        return START_STICKY
    }

    override fun onDestroy() {
        Log.d(this@FusedLocationTrackerService.javaClass.name, "The location tracker service has been stopped.")
        LocationServices.getFusedLocationProviderClient(this)
            .removeLocationUpdates(locationCallback)
        super.onDestroy()
    }

    companion object {
        const val LOCATION_TRACKER_INTERVAL = 15000L
    }

}

fun Context.startLocationTrackerService() {
    startService(Intent(this, FusedLocationTrackerService::class.java))
}

fun Context.stopLocationTrackerService() {
    stopService(Intent(this, FusedLocationTrackerService::class.java))
}
