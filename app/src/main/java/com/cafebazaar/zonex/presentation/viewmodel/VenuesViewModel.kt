package com.cafebazaar.zonex.presentation.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.cafebazaar.zonex.domain.LoadableData
import com.cafebazaar.zonex.domain.Location
import com.cafebazaar.zonex.domain.VenueDetail
import com.cafebazaar.zonex.domain.interactor.UpdateVenues
import com.cafebazaar.zonex.domain.repository.LocationRepository
import com.cafebazaar.zonex.domain.repository.VenueRepository
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.launch
import kotlinx.coroutines.withTimeout

/**
 * Created by Mina Alamolhoda ( minaalemalhoda@gmail.com ) on 2019-09-22.
 */

class VenuesViewModel constructor(
    private val venueRepository: VenueRepository,
    private val locationRepository: LocationRepository,
    private val updateVenues: UpdateVenues
) : BaseViewModel() {
    val venues = venueRepository.venues

    val state: MutableLiveData<VenuesState> by lazy {
        listenToLocationChange()
        MutableLiveData(
            VenuesState(
                locationRepository.getLastCachedInfoLocation(),
                LoadableData.NOT_INITIALIZED
            )
        )
    }

    val venueDetail = MutableLiveData(VenueDetailState(null, LoadableData.NOT_INITIALIZED))

    private fun listenToLocationChange() {
        viewModelScope.launch {
            onBg {
                locationRepository.getLocationUpdates().consumeEach {
                    updateVenues(it)
                }
            }

        }
    }


    fun updateVenues(location: Location? = null) {
        viewModelScope.launch {
            withTimeout(5000) {
                onBg {
                    runCatching<Any> {
                        val offset: Int
                        val currentLocation: Location?
                        if (location == null) {
                            currentLocation = locationRepository.getLastCachedInfoLocation()
                            offset = venues.value?.size ?: 0
                        } else {
                            currentLocation = location
                            offset = 0
                        }
                        applyVenueState {
                            copy(
                                location = currentLocation,
                                state = LoadableData.LOADING
                            )
                        }
                        if (currentLocation != null)
                            updateVenues.execute(
                                offset,
                                VENUE_PAGINATION_LIMIT,
                                currentLocation
                            )
                        else Result.failure<Any>(NoLocationException)
                    }.fold({
                        applyVenueState {
                            copy(
                                state = LoadableData.LOADED
                            )
                        }
                        Log.d(
                            this@VenuesViewModel.javaClass.name,
                            "Venues have been updated successfully."
                        )

                    }, {
                        applyVenueState {
                            copy(
                                state = LoadableData.FAILED
                            )
                        }
                        Log.e(
                            this@VenuesViewModel.javaClass.name,
                            "An exception occurs during updating venues....${it.message}"
                        )
                    })
                }
            }

        }

    }

    fun forceUpdateVenues() {
        val location = locationRepository.getLastKnownLocation()
        updateVenues(location)
    }


    private fun applyVenueState(function: VenuesState.() -> VenuesState) {
        state.applyState {
            function.invoke(this)
        }
    }

    private fun applyVenueDetailState(function: VenueDetailState.() -> VenueDetailState) {
        venueDetail.applyState {
            function.invoke(this)
        }
    }

    fun getVenueDetail(venueId: String) {
       venueDetail.value?.state?.takeIf { it == LoadableData.LOADING }?.let {
           return
       }
        viewModelScope.launch {
            applyVenueDetailState {
                copy(state = LoadableData.LOADING)
            }
            onBg {
                runCatching {
                    venueRepository.getVenueDetail(venueId)
                }.fold({ value ->
                    applyVenueDetailState {
                        copy(
                            venueDetail = value,
                            state = LoadableData.LOADED
                        )
                    }
                    Log.d(
                        this@VenuesViewModel.javaClass.name,
                        "Venue detail with id: $venueId has been successfully fetched."
                    )
                }, {
                    applyVenueDetailState {
                        copy(
                            state = LoadableData.FAILED
                        )
                    }
                    Log.d(
                        this@VenuesViewModel.javaClass.name,
                        "Unable to get venue detail id: $venueId...${it.message}."
                    )
                })
            }
        }

    }

    companion object {
        const val VENUE_PAGINATION_LIMIT = 10
        val NoLocationException = Exception("Can't access user location")
    }

}

data class VenueDetailState(val venueDetail: VenueDetail?, val state: LoadableData)
data class VenuesState(val location: Location?, val state: LoadableData)
