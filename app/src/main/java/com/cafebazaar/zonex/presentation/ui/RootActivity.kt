package com.cafebazaar.zonex.presentation.ui

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.provider.Settings
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.cafebazaar.zonex.R
import com.cafebazaar.zonex.domain.ConnectivityStatus
import com.cafebazaar.zonex.domain.GpsStatus
import com.cafebazaar.zonex.domain.interactor.StartLocationTrackerService
import com.cafebazaar.zonex.presentation.service.stopLocationTrackerService
import com.cafebazaar.zonex.presentation.ui.base.BaseActivity
import com.cafebazaar.zonex.presentation.viewmodel.RootViewModel
import com.cafebazaar.zonex.presentation.viewmodel.VenuesViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_root.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * Created by Mina Alamolhoda ( minaalemalhoda@gmail.com ) on 2019-09-17.
 */
class RootActivity : BaseActivity() {

    private val startLocationTrackerService: StartLocationTrackerService by inject()

    private val rootViewModel: RootViewModel by viewModel()
    /**
     * Shared view model uses parent ViewModelStore to resolve it,
     * This view model is injected in both [VenuesListFragment] and [VenueDetailFragment] as a shared view model
     */
    private val venuesViewModel: VenuesViewModel by viewModel()

    private val gpsWarningSnackBar by lazy {
        Snackbar.make(
            container,
            getString(R.string.gps_warning),
            Snackbar.LENGTH_INDEFINITE
        ).apply {
            setAction(getString(R.string.setting_action)) {
                startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
            }
        }
    }


    private val connectivityWarningSnackBar by lazy {
        Snackbar.make(
            container,
            getString(R.string.connectivity_warning),
            Snackbar.LENGTH_INDEFINITE
        ).apply {
            setAction(getString(R.string.setting_action)) {
                startActivity(Intent(Settings.ACTION_WIFI_SETTINGS))
            }
        }
    }

    private var errorSnackBar: Snackbar? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_root)
        checkForLocationPermission()
        listenToGpsStatus()
        listenToConnectivityStatus()

    }

    private fun checkForShowingStatusWarning() {
        if (rootViewModel.connectivityStatus.value == ConnectivityStatus.DISCONNECT)
            showConnectivityWarning()
        else if (rootViewModel.gpsStatus.value == GpsStatus.OFF)
            showGpsWarning()

    }

    private fun listenToGpsStatus() {
        rootViewModel.gpsStatus.observe(this, Observer { status ->
            when (status) {
                GpsStatus.ON -> {
                    hideGpsWarning()
                    startLocationTrackerService.execute()
                }
                else -> {
                    showGpsWarning()
                    stopLocationTrackerService()
                }
            }
        })
    }

    private fun showGpsWarning() {
        gpsWarningSnackBar.show()
    }

    private fun hideGpsWarning() {
        gpsWarningSnackBar.dismiss()
    }

    private fun listenToConnectivityStatus() {
        rootViewModel.connectivityStatus.observe(this, Observer { status ->
            when (status) {
                ConnectivityStatus.CONNECT -> {
                    hideConnectivityWarning()
                    startLocationTrackerService.execute()
                }
                else -> {
                    showConnectivityWarning()
                    stopLocationTrackerService()
                }
            }
        })
    }

    private fun showConnectivityWarning() {
        connectivityWarningSnackBar.show()
    }

    private fun hideConnectivityWarning() {
        connectivityWarningSnackBar.dismiss()
    }

    private fun checkForLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
            != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_PERMISSION_REQUEST_ID
            )
        } else startLocationTrackerService.execute()

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == LOCATION_PERMISSION_REQUEST_ID) {
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                startLocationTrackerService.execute()
            } else {
                showError(getString(R.string.location_permission_error))
            }
            return

        }
    }

    override fun onPause() {
        stopLocationTrackerService()
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
        if (!startLocationTrackerService.execute()) {
            showError(getString(R.string.locationtracker_error))
        } else hideError()
        checkForShowingStatusWarning()
    }

    private fun showError(string: String) {
        errorSnackBar = Snackbar.make(container, string, Snackbar.LENGTH_LONG).apply {
            show()
        }
    }

    private fun hideError() {
        errorSnackBar?.dismiss()
    }

    override fun onDestroy() {
        gpsWarningSnackBar.dismiss()
        connectivityWarningSnackBar.dismiss()
        errorSnackBar?.dismiss()
        super.onDestroy()
    }

    companion object {
        const val LOCATION_PERMISSION_REQUEST_ID = 12345
    }

}
