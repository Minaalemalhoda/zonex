package com.cafebazaar.zonex.presentation.service.microservice

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.location.LocationManager
import android.os.Handler
import android.os.HandlerThread
import android.util.Log
import com.cafebazaar.zonex.domain.GpsStatus
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.channels.sendBlocking
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow

/**
 * Created by Mina Alamolhoda ( minaalemalhoda@gmail.com ) on 2019-09-19.
 */

class GpsMicroService constructor(private val context: Context) :
    UserStatusMicroService<GpsStatus> {

    override val handler = Handler(HandlerThread("Gps Handler Thread").apply {
        start()
    }.looper)

    override fun statuses(): Flow<GpsStatus> = callbackFlow {
        val gpsStatusChangeReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                intent.action?.equals(LocationManager.PROVIDERS_CHANGED_ACTION)?.let {
                    val status = getCurrentStatus()
                    Log.d(this@GpsMicroService.javaClass.name, "The Gps status changed to $status.")
                    sendBlocking(status)
                }
            }

        }
        context.applicationContext.registerReceiver(
            gpsStatusChangeReceiver,
            intentFilter,
            null,
            handler
        )

        awaitClose {
            Log.d(this@GpsMicroService.javaClass.name, "The Gps receiver has been unregistered.")
            context.applicationContext.unregisterReceiver(gpsStatusChangeReceiver)
        }

    }

    override fun getCurrentStatus(): GpsStatus {
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER).let { isGpsOn ->
            if (isGpsOn) GpsStatus.ON else
                GpsStatus.OFF
        }
    }

    private val locationManager =
        context.getSystemService(Context.LOCATION_SERVICE) as LocationManager

    private val intentFilter = IntentFilter(LocationManager.PROVIDERS_CHANGED_ACTION)


}