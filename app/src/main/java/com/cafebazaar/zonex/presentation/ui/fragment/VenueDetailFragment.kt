package com.cafebazaar.zonex.presentation.ui.fragment

import android.content.Intent
import android.graphics.Typeface
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.cafebazaar.zonex.R
import com.cafebazaar.zonex.domain.VenueDetail
import com.cafebazaar.zonex.presentation.viewmodel.VenuesViewModel
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_venue_detail.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

/**
 * Created by Mina Alamolhoda ( minaalemalhoda@gmail.com ) on 2019-09-22.
 */

class VenueDetailFragment : BottomSheetDialogFragment() {

    private val viewModel: VenuesViewModel by sharedViewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_venue_detail, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.venueDetail.value?.venueDetail?.let { venueDetail ->
            bindVenueDetailInfo(venueDetail)
        }

    }

    private fun bindVenueDetailInfo(venueDetail: VenueDetail) {
        progressbar_venue_rating.progress = (venueDetail.rating * 10).toInt()
        textview_venue_rating.text = getString(R.string.venue_rating, venueDetail.rating.toString())
        textview_venue_id.text = venueDetail.name
        venueDetail.contact?.phoneNumber?.let { phone ->
            textview_venue_phone.enableTextView(phone)
        }
        venueDetail.locationInfo.address?.let { address ->
            textview_venue_address.enableTextView(address)
        }

        textview_venue_likes.text =
            getString(R.string.venue_likes, venueDetail.likes.count.toString())
        textview_venue_foursquare.setOnClickListener {
            val intent = Uri.parse(venueDetail.canonicalUrl).getIntent()
            startActivity(intent)
        }
    }

    private fun TextView.enableTextView(txt: String) {
        setTextColor(ContextCompat.getColor(context, R.color.textDark))
        typeface = Typeface.DEFAULT
        text = txt
    }


    private fun Uri.getIntent(flags: Int? = null): Intent {
        val intent = Intent(Intent.ACTION_VIEW, this)
        flags?.let {
            intent.addFlags(it)
        }
        return intent
    }
}

