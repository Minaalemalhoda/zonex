package com.cafebazaar.zonex.presentation.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cafebazaar.zonex.presentation.util.CoroutineUtility
import kotlinx.coroutines.withContext

/**
 * Created by Mina Alamolhoda ( minaalemalhoda@gmail.com ) on 2019-09-21.
 */

open class BaseViewModel : ViewModel() {

    val coroutineDispatcherProvider = CoroutineUtility.coroutineDispatcherProvider()

    suspend inline fun <T> onUI(crossinline coroutine: suspend () -> T): T {
        return withContext(coroutineDispatcherProvider.uiDispatcher()) {
            coroutine()
        }
    }

    suspend inline fun <T> onBg(crossinline coroutine: suspend () -> T): T {
        return withContext(coroutineDispatcherProvider.bgDispatcher()) {
            coroutine()
        }
    }

    suspend inline fun <T> onIO(crossinline coroutine: suspend () -> T): T {
        return withContext(coroutineDispatcherProvider.ioDispatcher()) {
            coroutine()
        }
    }

    fun<T> MutableLiveData<T>.applyState(function: T.() -> T) {
        val newState = this.value?.function()
        if (newState != this.value)
            Log.d(this@BaseViewModel.javaClass.name, "The new state is posted..$newState")
            this.postValue(newState)
    }
}