package com.cafebazaar.zonex.domain.interactor

import android.util.Log
import com.cafebazaar.zonex.domain.Location
import com.cafebazaar.zonex.domain.repository.VenueRepository

/**
 * Created by Mina Alamolhoda ( minaalemalhoda@gmail.com ) on 2019-09-22.
 */

class UpdateVenues constructor(private val venueRepository: VenueRepository) {

    suspend fun execute(offset: Int, limit: Int, location: Location) {
        val isNewLocation = offset == 0
        if (offset > FOURSQUARE_OFFSET_LIMIT) {
            Log.e(this.javaClass.name, "The given offset is invalid.")
            return
        }
        val newVenues = venueRepository.getVenues(location, offset, limit)
        if (isNewLocation) {
            venueRepository.updateCachedVenue(newVenues)
            Log.d(this.javaClass.name, "The venues cached date is reset for new location.")
        } else {
            venueRepository.insertCachedVenues(newVenues)
            Log.d(this.javaClass.name, "The venues cached data is updated.")
        }

    }

    companion object {
        const val FOURSQUARE_OFFSET_LIMIT = 50
    }
}
