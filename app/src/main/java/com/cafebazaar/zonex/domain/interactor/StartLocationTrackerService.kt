package com.cafebazaar.zonex.domain.interactor

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import androidx.core.content.ContextCompat
import com.cafebazaar.zonex.domain.ConnectivityStatus
import com.cafebazaar.zonex.domain.GpsStatus
import com.cafebazaar.zonex.presentation.service.startLocationTrackerService
import com.cafebazaar.zonex.presentation.service.microservice.ConnectivityStatusMicroService
import com.cafebazaar.zonex.presentation.service.microservice.GpsMicroService


/**
 * Created by Mina Alamolhoda ( minaalemalhoda@gmail.com ) on 2019-09-20.
 */
class StartLocationTrackerService constructor(
    private val context: Context,
    private val connectivityListener: ConnectivityStatusMicroService,
    private val gpsMicroService: GpsMicroService
) {
    fun execute(): Boolean {
        if (checkLocationPermission()) {
            if (gpsMicroService.getCurrentStatus() == GpsStatus.ON &&
                connectivityListener.getCurrentStatus() == ConnectivityStatus.CONNECT
            ) {
                context.startLocationTrackerService()
                return true
            }

        }

        return false
    }

    private fun checkLocationPermission() =
        Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                ContextCompat.checkSelfPermission(
                    context,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED
}