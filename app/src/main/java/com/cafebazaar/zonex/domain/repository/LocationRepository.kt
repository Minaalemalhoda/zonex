package com.cafebazaar.zonex.domain.repository

import com.cafebazaar.zonex.domain.Location
import kotlinx.coroutines.channels.ReceiveChannel

/**
 * Created by Mina Alamolhoda ( minaalemalhoda@gmail.com ) on 2019-09-20.
 */

interface LocationRepository {
    fun getLastKnownLocation(): Location?
    fun getLastCachedInfoLocation(): Location?
    fun submitLocation(newLocation: Location)
    fun cacheLocation(location: Location)
    fun getLocationUpdates(): ReceiveChannel<Location>
}