package com.cafebazaar.zonex.domain

/**
 * Created by Mina Alamolhoda ( minaalemalhoda@gmail.com ) on 2019-09-19.
 */

data class Location(val latitude: Float, val longitude: Float) {
    override fun toString(): String {
        return "$latitude,$longitude"
    }
}

enum class ConnectivityStatus {
    CONNECT,
    DISCONNECT
}

enum class GpsStatus {
    ON,
    OFF
}


data class Venue(
    val id: String,
    val name: String,
    val category: String,
    val locationInfo: VenueLocationInfo
)


data class VenueLocationInfo(val distance: Int, val address: String?)

enum class LoadableData {
    LOADING,
    LOADED,
    FAILED,
    NOT_INITIALIZED
}

data class VenueDetail(
    val id: String,
    val name: String,
    val locationInfo: VenueLocationInfo,
    val canonicalUrl: String,
    val likes: Likes,
    val rating: Float,
    val contact: Contact?
)

data class Likes(val count: Int, val summary: String)

data class Contact(val phoneNumber: String?, val instagram: String?)