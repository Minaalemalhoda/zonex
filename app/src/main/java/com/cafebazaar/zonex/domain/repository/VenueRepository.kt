package com.cafebazaar.zonex.domain.repository

import androidx.lifecycle.LiveData
import com.cafebazaar.zonex.domain.Location
import com.cafebazaar.zonex.domain.Venue
import com.cafebazaar.zonex.domain.VenueDetail

/**
 * Created by Mina Alamolhoda ( minaalemalhoda@gmail.com ) on 2019-09-22.
 */

interface VenueRepository {
    val venues: LiveData<List<Venue>>
    suspend fun getVenues(currentLocation: Location, offset: Int, limit: Int): List<Venue>
    suspend fun insertCachedVenues(venues: List<Venue>)
    suspend fun updateCachedVenue(venues: List<Venue>)
    suspend fun getVenueDetail(venueId: String): VenueDetail
}