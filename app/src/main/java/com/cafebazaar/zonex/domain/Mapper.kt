package com.cafebazaar.zonex.domain

/**
 * Created by Mina Alamolhoda ( minaalemalhoda@gmail.com ) on 2019-09-20.
 */

fun android.location.Location.mapToZonexLocation() =
    Location(latitude.toFloat(), longitude.toFloat())

fun Location.toAndroidLocation(): android.location.Location {
    val loc = android.location.Location("mock android location")
    loc.longitude = longitude.toDouble()
    loc.latitude = latitude.toDouble()
    return loc

}