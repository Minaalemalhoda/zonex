package com.cafebazaar.zonex.domain

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


/**
 * Created by Mina Alamolhoda ( minaalemalhoda@gmail.com ) on 2019-09-22.
 */
/**
 * Room disallows object references between entity classes
 * So extract th location info into two separate fields [distance], [address]
 */

@Entity
data class VenueEntity(
    @PrimaryKey @ColumnInfo(name = "id") val id: String,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "distance") val distance: Int,
    @ColumnInfo(name = "address") val address: String?,
    @ColumnInfo(name = "category") val category: String
)

