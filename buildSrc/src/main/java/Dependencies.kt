/**
 * Created by Mina Alamolhoda ( minaalemalhoda@gmail.com ) on 2019-09-17.
 */

object Versions {
    const val code = 1
    const val name = "1.0"
    const val minSdk = 17
    const val targetSdk = 29
    const val compileSdk = 29
    const val buildTools = "29.0.2"
    const val kotlin = "1.3.41"
    const val coreKtx = "1.1.0"
    const val junit = "4.12"
    const val runner = "1.2.0"
    const val espressoCore = "3.2.0"
    const val appCompat = "1.1.0"
    const val android = "3.5.0"
    const val retrofit = "2.6.1"
    const val koin = "2.0.1"
    const val coroutine = "1.3.1"
    const val gmsLibs = "17.0.0"
    const val lifecycle = "2.1.0"
    const val viewModelScope = "2.1.0-beta01"
    const val lifecycleScope = "2.2.0-alpha01"
    const val livedataScope = "2.2.0-alpha01"
    const val navigation ="2.1.0-alpha06"
    const val constraintLayout = "1.1.3"
    const val room = "2.2.0-rc01"
    const val material = "1.0.0"
}

object Deps {
    const val kotlin = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Versions.kotlin}"
    const val coreKtx = "androidx.core:core-ktx:${Versions.coreKtx}"
    const val junit = "junit:junit:${Versions.junit}"
    const val runner = "androidx.test:runner:${Versions.runner}"
    const val espressoCore = "androidx.test.espresso:espresso-core:${Versions.espressoCore}"
    const val appCompat = "androidx.appcompat:appcompat:${Versions.appCompat}"
    const val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit}"
    const val retrofitGsonConverter = "com.squareup.retrofit2:converter-gson:${Versions.retrofit}"
    const val koin = "org.koin:koin-android:${Versions.koin}"
    const val koinViewModel = "org.koin:koin-androidx-viewmodel:${Versions.koin}"
    const val coroutine = "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.coroutine}"
    const val googleLocation = "com.google.android.gms:play-services-location:${Versions.gmsLibs}"
    const val lifecycle = "androidx.lifecycle:lifecycle-extensions:${Versions.lifecycle}"
    const val viewModelScope = "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.viewModelScope}"
    const val lifecycleScope = "androidx.lifecycle:lifecycle-runtime-ktx:${Versions.lifecycleScope}"
    const val liveDataScope = "androidx.lifecycle:lifecycle-livedata-ktx:${Versions.livedataScope}"
    const val navigation = "androidx.navigation:navigation-fragment-ktx:${Versions.navigation}"
    const val navigationKtx = "androidx.navigation:navigation-ui-ktx:${Versions.navigation}"
    const val constraintLayout = "androidx.constraintlayout:constraintlayout:${Versions.constraintLayout}"
    const val room = "androidx.room:room-runtime:${Versions.room}"
    const val roomCompiler = "androidx.room:room-compiler:${Versions.room}"
    const val roomKtx = "androidx.room:room-ktx:${Versions.room}"
    const val roomCoroutine = "androidx.room:room-coroutines:${Versions.room}"
    const val material = "com.google.android.material:material:${Versions.material}"
}

object Plugins {
    const val kotlinPlugin = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlin}"
    const val androidPlugin = "com.android.tools.build:gradle:${Versions.android}"
}